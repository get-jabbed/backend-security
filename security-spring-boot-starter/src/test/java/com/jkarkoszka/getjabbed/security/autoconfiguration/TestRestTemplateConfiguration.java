package com.jkarkoszka.getjabbed.security.autoconfiguration;

import com.jkarkoszka.getjabbed.security.Role;
import com.jkarkoszka.getjabbed.security.autoconfigure.SecurityProperties;
import com.jkarkoszka.getjabbed.security.filter.GetJabbedAuthenticationToken;
import com.jkarkoszka.getjabbed.security.service.JwtTokenGeneratorService;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.web.client.RestTemplate;

@Configuration
public class TestRestTemplateConfiguration {

  @Value("${server.port}")
  private int port;

  @Autowired
  JwtTokenGeneratorService jwtTokenGeneratorService;

  @Autowired
  SecurityProperties securityProperties;

  @Bean
  public RestTemplate testRestTemplateAsVaccineCenter() {
    return new RestTemplateBuilder()
        .rootUri("http://localhost:" + port)
        .defaultHeader("Authorization", "Bearer " + generateVaccineUserToken("admin123", "100"))
        .build();
  }

  @Bean
  public RestTemplate testRestTemplateAsPatient() {
    return new RestTemplateBuilder()
        .rootUri("http://localhost:" + port)
        .defaultHeader("Authorization", "Bearer " + generatePatientToken("user123"))
        .build();
  }

  @Bean
  public RestTemplate testRestTemplateAsBackendService() {
    return new RestTemplateBuilder()
        .rootUri("http://localhost:" + port)
        .defaultHeader("Authorization", "Bearer " + securityProperties.getBackendServiceToken())
        .build();
  }

  @Bean
  public RestTemplate testRestTemplateNoUser() {
    return new RestTemplateBuilder()
        .rootUri("http://localhost:" + port)
        .build();
  }

  private String generatePatientToken(String username) {
    return generateAuthenticationToken(username, Role.ROLE_PATIENT.name(), null);
  }

  private String generateVaccineUserToken(String username, String vaccineUserId) {
    return generateAuthenticationToken(username, Role.ROLE_VACCINE_CENTER.name(), vaccineUserId);
  }

  private String generateAuthenticationToken(String username, String role, String vaccineUserId) {
    return jwtTokenGeneratorService.createToken(
        new GetJabbedAuthenticationToken(username, StringUtils.EMPTY,
            List.of(new SimpleGrantedAuthority(role)), vaccineUserId));
  }
}
