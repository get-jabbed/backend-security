package com.jkarkoszka.getjabbed.security.autoconfiguration;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import com.jkarkoszka.getjabbed.security.filter.JwtTokenAuthenticationFilter;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import testproject.TestApplication;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT,
    classes = {TestApplication.class, TestRestTemplateConfiguration.class})
class SecurityAutoConfigurationTest {

  @Autowired
  JwtTokenAuthenticationFilter jwtTokenAuthenticationFilter;

  @Autowired
  PasswordEncoder passwordEncoder;

  @Test
  void shouldCreateJwtTokenAuthenticationFilter() {
    assertThat(jwtTokenAuthenticationFilter).isNotNull();
  }

  @Test
  void shouldCreatePasswordEncoder() {
    assertThat(passwordEncoder).isNotNull();
  }
}
