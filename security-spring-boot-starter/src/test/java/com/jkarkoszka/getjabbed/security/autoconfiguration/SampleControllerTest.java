package com.jkarkoszka.getjabbed.security.autoconfiguration;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import testproject.TestApplication;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT,
    classes = {TestApplication.class, TestRestTemplateConfiguration.class})
public class SampleControllerTest {

  @Autowired
  RestTemplate testRestTemplateAsVaccineCenter;

  @Autowired
  RestTemplate testRestTemplateAsPatient;

  @Autowired
  RestTemplate testRestTemplateNoUser;

  @Autowired
  RestTemplate testRestTemplateAsBackendService;

  @Test
  void shouldReturnVaccineCenterIfCredentialsAreValid() {
    //when
    var response = testRestTemplateAsVaccineCenter.getForEntity("/vaccine-center", String.class);

    //then
    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo("vaccine-center");
  }

  @Test
  void shouldReturnPatientIfCredentialsAreValid() {
    //when
    var response = testRestTemplateAsPatient.getForEntity("/patient", String.class);

    //then
    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo("patient");
  }

  @Test
  void shouldReturnOpenIfThereIsNoCredentials() {
    //when
    var response = testRestTemplateNoUser.getForEntity("/open", String.class);

    //then
    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo("open");
  }

  @Test
  void shouldReturnError401IfThereIsNoCredentialsOnPatientEndpoint() {
    //when//then
    try {
      testRestTemplateNoUser.getForEntity("/patient", String.class);
    } catch (HttpClientErrorException httpClientErrorException) {
      assertThat(httpClientErrorException.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
    }
  }

  @Test
  void shouldReturnError403IfThereInvalidCredentialsOnPatientEndpoint() {
    //when//then
    try {
      testRestTemplateAsVaccineCenter.getForEntity("/patient", String.class);
    } catch (HttpClientErrorException httpClientErrorException) {
      assertThat(httpClientErrorException.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    }
  }

  @Test
  void shouldReturnError401IfThereIsNoCredentialsOnVaccineCenterEndpoint() {
    //when//then
    try {
      testRestTemplateNoUser.getForEntity("/vaccine-center", String.class);
    } catch (HttpClientErrorException httpClientErrorException) {
      assertThat(httpClientErrorException.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
    }
  }

  @Test
  void shouldReturnError403IfThereInvalidCredentialsOnVaccineCenterEndpoint() {
    //when//then
    try {
      testRestTemplateAsPatient.getForEntity("/vaccine-center", String.class);
    } catch (HttpClientErrorException httpClientErrorException) {
      assertThat(httpClientErrorException.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    }
  }

  @Test
  void shouldReturnOpenIfPatientCredentialsProvided() {
    //when
    var response = testRestTemplateAsPatient.getForEntity("/open", String.class);

    //then
    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo("open");
  }

  @Test
  void shouldReturnOpenIfVaccineCenterCredentialsProvided() {
    //when
    var response = testRestTemplateAsVaccineCenter.getForEntity("/open", String.class);

    //then
    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo("open");
  }

  @Test
  void shouldReturnBackendServiceIfBackendServiceCredentialsProvided() {
    //when
    var response = testRestTemplateAsBackendService.getForEntity("/backend-service", String.class);

    //then
    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo("backend-service");
  }

  @Test
  void shouldReturnError403IfThereInvalidCredentialsOnBackendServiceEndpoint() {
    //when//then
    try {
      testRestTemplateAsPatient.getForEntity("/backend-service", String.class);
    } catch (HttpClientErrorException httpClientErrorException) {
      assertThat(httpClientErrorException.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    }
  }

  @Test
  void shouldReturnError401IfThereNoCredentialsOnBackendServiceEndpoint() {
    //when//then
    try {
      testRestTemplateNoUser.getForEntity("/backend-service", String.class);
    } catch (HttpClientErrorException httpClientErrorException) {
      assertThat(httpClientErrorException.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
    }
  }
}
