package testproject;

import static com.jkarkoszka.getjabbed.security.Role.ROLE_BACKEND_SERVICE;
import static com.jkarkoszka.getjabbed.security.Role.ROLE_PATIENT;
import static com.jkarkoszka.getjabbed.security.Role.ROLE_VACCINE_CENTER;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.web.server.SecurityWebFilterChain;

@Configuration
public class SecurityConfiguration {

  @Bean
  SecurityWebFilterChain springWebFilterChain(ServerHttpSecurity http) {
    return http
        .authorizeExchange(this::prepareAuthorizeExchange)
        .build();
  }

  private ServerHttpSecurity.AuthorizeExchangeSpec prepareAuthorizeExchange(
      ServerHttpSecurity.AuthorizeExchangeSpec it) {
    return it
        .pathMatchers(HttpMethod.GET, "/patient").hasRole(ROLE_PATIENT.getValue())
        .pathMatchers(HttpMethod.GET, "/vaccine-center").hasRole(ROLE_VACCINE_CENTER.getValue())
        .pathMatchers(HttpMethod.GET, "/backend-service").hasRole(ROLE_BACKEND_SERVICE.getValue())
        .anyExchange().permitAll();
  }
}
