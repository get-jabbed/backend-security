package testproject;

import com.jkarkoszka.getjabbed.security.filter.GetJabbedAuthenticationToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SampleController {

  @GetMapping("/patient")
  public String user() {
    return "patient";
  }

  @GetMapping("/vaccine-center")
  public String vaccineCenter(GetJabbedAuthenticationToken getJabbedAuthenticationToken) {
    return "vaccine-center";
  }

  @GetMapping("/open")
  public String open() {
    return "open";
  }

  @GetMapping("/backend-service")
  public String backendService() {
    return "backend-service";
  }

}
