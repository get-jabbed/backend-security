package com.jkarkoszka.getjabbed.security;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Available roles.
 */
@AllArgsConstructor
@Getter
public enum Role {
  ROLE_PATIENT("PATIENT"), ROLE_VACCINE_CENTER("VACCINE_CENTER"),
  ROLE_BACKEND_SERVICE("BACKEND_SERVICE");

  private String value;
}
