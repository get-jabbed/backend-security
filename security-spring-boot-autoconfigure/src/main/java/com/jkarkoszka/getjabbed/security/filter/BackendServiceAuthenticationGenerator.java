package com.jkarkoszka.getjabbed.security.filter;

import com.jkarkoszka.getjabbed.security.Role;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

/**
 * Class to generate and unpack JWT token.
 */
@AllArgsConstructor
@Slf4j
class BackendServiceAuthenticationGenerator {

  public static final String PRINCIPAL = "BACKEND_SERVICE";

  static Authentication generate() {
    return new UsernamePasswordAuthenticationToken(PRINCIPAL, StringUtils.EMPTY,
        List.of(new SimpleGrantedAuthority(Role.ROLE_BACKEND_SERVICE.name())));
  }
}
