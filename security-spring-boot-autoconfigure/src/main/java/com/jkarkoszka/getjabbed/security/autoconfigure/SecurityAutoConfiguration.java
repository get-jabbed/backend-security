package com.jkarkoszka.getjabbed.security.autoconfigure;

import com.jkarkoszka.getjabbed.security.filter.BackendServiceTokenAuthenticationFilter;
import com.jkarkoszka.getjabbed.security.filter.JwtTokenAuthenticationFilter;
import com.jkarkoszka.getjabbed.security.service.JwtTokenGeneratorService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.SecurityWebFiltersOrder;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.server.context.NoOpServerSecurityContextRepository;

/**
 * Autoconfiguration for security.
 */
@Configuration
@EnableConfigurationProperties(SecurityProperties.class)
@Slf4j
@AllArgsConstructor
@EnableWebFluxSecurity
public class SecurityAutoConfiguration {

  @Bean
  JwtTokenGeneratorService jwtTokenGeneratorService(SecurityProperties securityProperties) {
    return new JwtTokenGeneratorService(securityProperties);
  }

  @Bean
  JwtTokenAuthenticationFilter jwtTokenAuthenticationFilter(
      JwtTokenGeneratorService jwtTokenGeneratorService) {
    return new JwtTokenAuthenticationFilter(jwtTokenGeneratorService);
  }

  @Bean
  BackendServiceTokenAuthenticationFilter backendServiceTokenAuthenticationFilter(
      SecurityProperties securityProperties) {
    return new BackendServiceTokenAuthenticationFilter(securityProperties);
  }

  @Bean
  public PasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }

  @Bean
  @Primary
  ServerHttpSecurity http(
      ServerHttpSecurity http,
      JwtTokenAuthenticationFilter jwtTokenAuthenticationFilter,
      BackendServiceTokenAuthenticationFilter backendServiceTokenAuthenticationFilter) {
    return http
        .csrf(ServerHttpSecurity.CsrfSpec::disable)
        .httpBasic(ServerHttpSecurity.HttpBasicSpec::disable)
        .securityContextRepository(NoOpServerSecurityContextRepository.getInstance())
        .addFilterAt(jwtTokenAuthenticationFilter, SecurityWebFiltersOrder.HTTP_BASIC)
        .addFilterAt(backendServiceTokenAuthenticationFilter, SecurityWebFiltersOrder.HTTP_BASIC);
  }
}
