package com.jkarkoszka.getjabbed.security.filter;

import java.util.Collection;
import lombok.Getter;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

/**
 * Custom token with project specfic fields.
 */
@Getter
public class GetJabbedAuthenticationToken extends UsernamePasswordAuthenticationToken {

  private final String vaccineCenterId;

  public GetJabbedAuthenticationToken(Object principal, Object credentials,
                                      Collection<? extends GrantedAuthority> authorities,
                                      String vaccineCenterId) {
    super(principal, credentials, authorities);
    this.vaccineCenterId = vaccineCenterId;
  }
}
