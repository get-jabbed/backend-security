package com.jkarkoszka.getjabbed.security.autoconfigure;

import javax.validation.constraints.NotEmpty;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

/**
 * Class with the backend-db configuration.
 */
@ConfigurationProperties(prefix = "getjabbed.security")
@Data
@Validated
public class SecurityProperties {

  @NotEmpty
  private String jwtSecretKey;
  private long validityInMs = 3600000;
  @NotEmpty
  private String backendServiceToken;

}
