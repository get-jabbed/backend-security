package com.jkarkoszka.getjabbed.security.service;

import static java.util.stream.Collectors.joining;

import com.jkarkoszka.getjabbed.security.autoconfigure.SecurityProperties;
import com.jkarkoszka.getjabbed.security.filter.GetJabbedAuthenticationToken;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Date;
import java.util.Objects;
import javax.crypto.SecretKey;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;

/**
 * Class to generate and unpack JWT token.
 */
@AllArgsConstructor
@Slf4j
public class JwtTokenGeneratorService {

  private static final String AUTHORITIES_KEY = "roles";
  private static final String VACCINE_CENTER_ID_KEY = "vaccineCenterId";
  private final SecurityProperties securityProperties;

  /**
   * Method to generate JWT token.
   *
   * @param getJabbedAuthenticationToken getJabbedAuthenticationToken
   * @return jwt token
   */
  public String createToken(GetJabbedAuthenticationToken getJabbedAuthenticationToken) {
    var username = getJabbedAuthenticationToken.getName();
    var authorities = getJabbedAuthenticationToken.getAuthorities();
    var vaccineCenterId = getJabbedAuthenticationToken.getVaccineCenterId();
    var claims = Jwts.claims().setSubject(username);
    claims.put(AUTHORITIES_KEY,
        authorities.stream().map(GrantedAuthority::getAuthority).collect(joining(",")));
    claims.put(VACCINE_CENTER_ID_KEY, vaccineCenterId);
    var now = new Date();
    var validity = new Date(now.getTime() + securityProperties.getValidityInMs());
    return Jwts.builder()//
        .setClaims(claims)//
        .setIssuedAt(now)//
        .setExpiration(validity)//
        .signWith(prepareSecretKey(), SignatureAlgorithm.HS256)//
        .compact();
  }

  /**
   * Method to unpack JWT token.
   *
   * @param token token
   * @return Authentication
   */
  public Authentication getAuthentication(String token) {
    var claims = Jwts.parserBuilder().setSigningKey(prepareSecretKey()).build()
        .parseClaimsJws(token)
        .getBody();
    var authorities =
        AuthorityUtils.commaSeparatedStringToAuthorityList(claims.get(AUTHORITIES_KEY).toString());
    var principal = new User(claims.getSubject(), StringUtils.EMPTY, authorities);
    return new GetJabbedAuthenticationToken(principal, token, authorities,
        getVaccineCenterId(claims));
  }

  private String getVaccineCenterId(Claims claims) {
    if (Objects.nonNull(claims.get(VACCINE_CENTER_ID_KEY))) {
      return claims.get(VACCINE_CENTER_ID_KEY).toString();
    }
    return StringUtils.EMPTY;
  }

  /**
   * Method to validate JWT token.
   *
   * @param token JWT token
   * @return if token is valid
   */
  public boolean validateToken(String token) {
    try {
      var claims = Jwts.parserBuilder().setSigningKey(prepareSecretKey()).build()
          .parseClaimsJws(token);
      return !claims.getBody().getExpiration().before(new Date());
    } catch (JwtException | IllegalArgumentException e) {
      log.info(String.format("Invalid JWT token: %s", token));
    }
    return false;
  }

  private SecretKey prepareSecretKey() {
    var secret = Base64.getEncoder()
        .encodeToString(securityProperties.getJwtSecretKey().getBytes(StandardCharsets.UTF_8));
    return Keys.hmacShaKeyFor(secret.getBytes(StandardCharsets.UTF_8));
  }
}
