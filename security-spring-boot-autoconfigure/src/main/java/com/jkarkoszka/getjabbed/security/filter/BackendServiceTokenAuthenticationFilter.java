package com.jkarkoszka.getjabbed.security.filter;

import com.jkarkoszka.getjabbed.security.autoconfigure.SecurityProperties;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.util.StringUtils;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;
import reactor.core.publisher.Mono;

/**
 * Filter to check backend to backend token.
 */
@AllArgsConstructor
public class BackendServiceTokenAuthenticationFilter implements WebFilter {

  public static final String HEADER_PREFIX = "Bearer ";
  private final SecurityProperties securityProperties;

  @Override
  public Mono<Void> filter(ServerWebExchange exchange, WebFilterChain chain) {
    var token = resolveToken(exchange.getRequest());
    if (StringUtils.hasText(token) && securityProperties.getBackendServiceToken().equals(token)) {
      var authentication = BackendServiceAuthenticationGenerator.generate();
      return chain.filter(exchange)
          .contextWrite(ReactiveSecurityContextHolder.withAuthentication(authentication));
    }
    return chain.filter(exchange);
  }

  private String resolveToken(ServerHttpRequest request) {
    String bearerToken = request.getHeaders().getFirst(HttpHeaders.AUTHORIZATION);
    if (StringUtils.hasText(bearerToken) && bearerToken.startsWith(HEADER_PREFIX)) {
      return bearerToken.substring(7);
    }
    return null;
  }
}
